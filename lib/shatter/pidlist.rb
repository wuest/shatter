require 'funtools'
require 'thread'

module Shatter
  class Pidlist
    include Enumerable

    # Public: Initialize the internal state of the Pidlist.
    def initialize
      @known = []
      @mutex = Mutex.new
    end

    # Public: Iterate through known Pids.
    #
    # Yields each item in the list of known Pids.
    #
    # Returns nothing.
    def each(&block)
      @known.each(&block)
    end

    # Public: Add a Pid to the list of known Pids.
    #
    # pid - Pid to be tracked.
    #
    # Returns nothing.
    def insert(pid)
      mutex.synchronize do
        known << pid
      end
    end

    # Internal: Create a method which allows for fetching a Pid by a given
    # attribute.
    #
    # Signature
    #
    #   by_<kind>
    #
    # kind - String containing the name of the value by which to select.
    def self.build_fetch_by(kind)
      define_method("by_#{kind}") do |val|
        select { |pid| pid.send(kind) == val }
      end
    end

    build_fetch_by('name')
    build_fetch_by('pid')
    build_fetch_by('host')
    build_fetch_by('port')

    # insert :: Pid -> [Pid]
    settype(:insert, Pid, Array)

    private

    attr_reader :known, :mutex
  end
end
