require 'socket'

module Shatter
  class Pid < Struct.new(:pid, :host, :port, :box, :name)
    # Public: Open a socket to a given Controller and send any number of
    # messages to it.
    #
    # messages - Any number of arguments which will constitute a message to be
    #            sent to the Controller.
    #
    # Returns nothing.
    def pass(*messages)
      socket  = TCPSocket.new(host, port)
      nonce   = RbNaCl::Random.random_bytes(RbNaCl::SecretBox::NONCEBYTES)
      message = box.encrypt(nonce, Marshal.dump(messages))
      socket.send(Marshal.dump([nonce, message]), 0)
      socket.close
    end

    # Public: Represent the Pid as a String.
    #
    # Returns a String.
    def inspect
      "<#{pid} #{host}:#{port}#{" (#{name})" unless name.empty?}>"
    end
    alias :to_s :inspect
  end
end
