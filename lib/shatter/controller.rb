require 'shatter/pid'
require 'shatter/pidlist'
require 'socket'
require 'thread'

module Shatter
  class Controller
    attr_reader :parent

    # Public: Initialize the Controller, setting up the listening Socket and
    # thread to manage the mailbox.
    #
    # parent - Pid of the parent Controller, if applicable.
    # key    - String containing the shared secret for an RbNaCl SecretBox.
    def initialize(parent, key)
      @parent  = parent
      @box     = RbNaCl::SecretBox.new(key)
      @socket  = listen
      @mailbox = Queue.new
      @known   = Shatter::Pidlist.new
      @chunks  = {}

      pass(@parent, :system, [:childpid, pid]) if @parent
      Thread.new { mailbox_loop(key) }
    end

    # Public: Pass items in the current mailbox to a given block, removing them
    # permanently from the mailbox unless the item doesn't match any pattern in
    # the block.
    #
    # Returns nothing.
    def receive(&block)
      newbox = Queue.new
      item = @mailbox.pop
      instance_exec { block.(item) }
    rescue NoMatch
      newbox << item
      retry
    ensure
      restore_mailbox(@mailbox, newbox, nil)
    end

    # Public: Return the Pid representing the Controller, initializing the
    # struct if necessary.
    #
    # Returns a Pid.
    def pid
      unless @pid
        _, port, _, ip = @socket.addr.map(&:freeze)
        @pid           = Shatter::Pid.new($$, ip, port, @box, '')
      end
      @pid
    end

    private

    # Internal: Accept connections on the listening socket, accepting messages
    # and putting them into the mailbox.  This should be run within its own
    # thread.
    #
    # key - String containing the shared secret for an RbNaCl SecretBox.
    #
    # Does not return.
    deftail :mailbox_loop do |key|
      connection = @socket.accept
      Thread.new do
        data = connection.read
        begin
          nonce, ciphertext = Marshal.load(data)
          message = Marshal.load(@box.decrypt(nonce, ciphertext))
          @mailbox << message unless handle_message(message)
        rescue ArgumentError, RbNaCl::CryptoError, RbNaCl::LengthError
        end
      end
      mailbox_loop
    end

    # Internal: Open a listening socket on the first available port within a
    # given range.
    #
    # socket - TCPServer if one has been established, or nil. (default: nil)
    # range  - Range of ports to try to bind to. (default: 9479..9749)
    # port   - Fixnum indicating the port to attempt to bind to.
    #          (default: lowest port in range)
    #
    # Returns a listening TCPSocket.
    deftail :listen do |socket=nil, range=PORTRANGE, port=range.min|
      socket ? socket : listen(get_socket(port, range.max), range, port+1)
    end

    # Internal: Restore the state of a mailbox from a mailbox and temporary
    # mailbox, first emptying the mailbox into the temporary queue and then
    # vise versa, keeping the order consistent.
    #
    # a - Queue representing the original mailbox.
    # b - Queue representing the temporary target mailbox.
    # c - Status indication - true indicating that the original mailbox is
    #     ready to be filled, with any other value indicating that the original
    #     mailbox is still in need of being emptied.
    #
    # Returns nothing.
    defpattern :restore_mailbox do
      restore_mailbox(nil, nil, true) do |a,b,_|
        drain_mailbox(a, b)
      end
      restore_mailbox(nil, nil, nil)  do |a,b,_|
        restore_mailbox(b, a, drain_mailbox(a, b))
      end
    end

    # Internal: Check for any messages which are meant to be handled by the
    # system itself (indicated by [:system, [:type, payload]] structure).
    # Handle any which are encountered, or else return false.  Handlers must
    # return a truthy value to avoid the system message being added to the
    # mailbox.
    #
    # message - Message to be checked.
    #
    # Returns false unless a message is handled.
    defpattern :handle_message do
      handle_message([:system, [:childpid, nil]]) { |_, pid| add_pid(pid.last) }
      handle_message(nil)                         { |_| false }
    end

    # Internal: Add a Pid of another Controller to a list of known Pids.
    #
    # pid - Pid representing another Controller.
    #
    # Returns the Pid.
    def add_pid(pid)
      @known.insert(pid)
    end

    # Internal: Remove all messages from a given source Queue, inserting them
    # into a target Queue.
    #
    # source - Queue from which to drain messages.
    # target - Queue into which messages should be pushed.
    #
    # Returns true once source is empty.
    def drain_mailbox(source, target)
      loop { target << source.pop(true) }
    rescue ThreadError
      true
    end

    # Internal: Attempt to start a TCPServer on a given port.
    #
    # port - Fixnum indicating the port to which to attempt to bind.
    # max  - Fixnum representing the maximum port allowable.
    #
    # Raises StandardError if port is greater than max.
    #
    # Returns a TCPServer if bound, or nil if the port is in use.
    def get_socket(port, max)
      raise StandardError, "Cannot allocate a port" if port > max

      TCPServer.open('127.0.0.1', port)
    rescue Errno::EADDRINUSE
      nil
    end

    # listen :: Maybe Socket -> Range -> Fixnum -> Socket
    settype(:listen, [IPSocket, NilClass], Range, Fixnum, IPSocket)
    # restore_mailbox :: Queue -> Queue -> Bool -> Bool
    settype(:restore_mailbox, Queue, Queue, [TrueClass, NilClass], TrueClass)
    # drain_mailbox :: Queue -> Queue -> Bool
    settype(:drain_mailbox, Queue, Queue, TrueClass)
    # get_socket :: Fixnum -> Fixnum -> Maybe Socket
    settype(:get_socket, Fixnum, Fixnum, [NilClass, IPSocket])
  end
end
