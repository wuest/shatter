require 'funtools'
require 'rbnacl'
require 'rbnacl/libsodium'
require 'shatter/controller'

module Shatter
  extend self
  VERSION   = '0.0.2'
  PORTRANGE = 9479..9749

  controller = nil
  set        = ->(new_controller) { controller ||= new_controller }

  get_random = ->(varname, size = RbNaCl::PasswordHash::SCrypt::SALTBYTES) do
    if ENV[varname].to_s.length == size
      ENV[varname].force_encoding('BINARY')
    else
      RbNaCl::Random.random_bytes(size)
    end
  end

  salt = get_random.('SHATTER_SALT')
  pass = get_random.('SHATTER_PASS')
  size = RbNaCl::SecretBox::KEYBYTES
  key  = RbNaCl::PasswordHash.scrypt(pass, salt, 2**20, 2**40, size)

  # Public: Set the Controller for the currently running process.  This will be
  # used directly if the current process is expected to be passed messages.
  #
  # parent - Pid to which to report the process's new Pid. (default: nil)
  #
  # Returns nothing.
  define_method(:init)    { |parent=nil| set.(Controller.new(parent, key)) }

  # Public: Clear the process's Controller, then call init to set a new one.
  # This is needed after calling fork in order to obtain a new socket.
  #
  # parent - Pid representing the parent process from which the new process has
  #          been forked.
  #
  # Returns nothing.
  define_method(:chip)    { |parent| controller=nil; init(parent) }

  # Public: Wrap Controller#receive, passing a given block to the current
  # Controller.
  #
  # block - Proc which will process messages passed to it from the Controller's
  #         mailbox.
  #
  # Returns the result of block being passed messages from Controller's mailbox.
  define_method(:receive) { |&block| controller.receive(&block) }

  # Public: Call a given method on a Module, passing in given arguments.
  #
  # mod  - Module which contains the method referenced by fun.
  # fun  - Symbol or String referencing a method on the Module provided.
  # args - Arguments to be passed to the method.
  #
  # Returns the result of invoking fun(*args) on mod.
  define_method(:mfa)     { |mod, fun, args| mod.send(fun, *args) }

  # Public - Fetch the current Controllers pid if it is set.
  #
  # Returns a Pid or else nil.
  define_method(:pid)     { controller.pid if controller }

  # Public - Return the Pid of the process which spawned the current process if
  # one exists.
  #
  # Returns a Pid or else nil.
  define_method(:parent)  { controller.parent if controller }
end

module Kernel
  private

  # Public: Fork a new process to perform a given job.
  #
  # mod  - Module which contains the method referenced by fun.
  # fun  - Symbol or String referencing a method on the Module provided.
  # args - Arguments to be passed to the method.
  #
  # Returns nothing.
  def chip(mod, fun, *args)
    forked = fork do
      Shatter.chip(Shatter.pid)
      Shatter.mfa(mod, fun, args)
    end
    Process::detach(forked)
  end

  # Public: Return the current Controller's Pid if applicable.
  #
  # Returns a Pid or else nil.
  def pid
    Shatter.pid
  end

  # Public: Wrap Pid#pass.
  #
  # Returns nothing.
  def pass(target, *messages)
    target.pass(*messages)
  end

  # Public: Return the current Controller's Parent's Pid if applicable.
  #
  # Returns a Pid or else nil.
  def parent
    Shatter.parent
  end

  match_proc = ->(a, b) do
    if([a,b].map { |o| o.is_a?(Enumerable) && a.class == b.class }.all?)
      raise ArgumentError unless a.length == b.length

      zipped = a.is_a?(Hash) ? a.sort.zip(b.sort) : a.zip(b)

      zipped.reduce(true) do |c, e|
        c && match.(*e)
      end
    else
      a.nil? || a == b
    end
  end

  # Public: Define a pattern to be matched within the context of a receive
  # block, providing a block to execute when pattern is matched.
  #
  # l - Any number of arguments to define a pattern against which to match.
  # b - Proc to pass the arguments to if all pattern matches succeed.
  #
  # Raises SyntaxError if the thread is not called within the context of a
  # receive block (as signified by the thread variable :matches).
  #
  # Returns nothing.
  define_method :match do |*l, &b|
    unless Thread.current[:matches].is_a?(Array)
      raise(SyntaxError, 'syntax error, unexpected match')
    end

    Thread.current[:matches] << ->(n) do
     ->(*m) do
       if m.length == n.length
         raise NoMatch unless m.zip(n).map { |e| match_proc.(*e) }.all?
         instance_exec(*n, &b)
       end
     end.(*l)
    end
  end

  # Public: Build a pattern list and wrap Controller#receive, receiving items
  # from the Controller's mailbox and responding to them as appropriate.
  #
  # block - Proc containing pattern definitons.
  #
  # Returns nothing.
  def receive(&block)
    old_matches = Thread.current[:matches]
    Thread.current[:matches] = []

    yield

    # item is always an array due to the way Controller#receive and Pid#pass
    # work
    Shatter.receive do |item|
      Thread.current[:matches].each do |pattern|
        begin
          return instance_exec(item, &pattern)
        rescue NoMatch
        end
      end
      instance_exec { raise NoMatch }
    end

    Thread::current[:matches] = old_matches
  end
end
