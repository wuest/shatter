#!/usr/bin/env ruby
$:.unshift(File.expand_path('../lib'))
require 'shatter'

module Factorial
  extend self
  # Public: Spawn an arbitrary number of processes to compute a factorial.
  #
  # ret      - Pid to which to send the answer.
  # children - Number of child processes to spawn.
  # target   - Number representing the factorial to be calculated.
  def init(ret, children, target)
    ranges(target, children).each do |range|
      chip(Factorial, :reducer, pid, range)
    end

    accumulate(ret, children)
  end

  # Public: Wait to receive all children's answers, then pass the product of
  # them to a given Pid.
  #
  # ret      - Pid to which to send the answer.
  # children - Number of children for which to wait.
  # answers  - Array containing answers received so far. (default: [])
  deftail :accumulate do |ret, children, answers=[]|
    receive do
      match(:result, nil) do |_, answer|
        if children <= answers.length + 1
          pass(ret, :answer, answers.reduce(answer, &:*))
        else
          accumulate(ret, children, answers +  [answer])
        end
      end
    end
  end

  # Public: Send the result of the product of a range to a given Pid.
  #
  # ret   - Pid to which to send the answer.
  # range - Range which should be multiplied.
  def reducer(ret, range)
    pass(ret, :result, range.reduce(&:*))
  end

  private

  # Internal: Given a maximum number, split the range from 1 to that number
  # into a given number of Ranges of roughly equal size.
  #
  # max     - Maximum number to split into ranges.
  # total   - Number of Ranges to produce.
  # results - Array of Ranges produced so far. (default: [])
  #
  # Returns an Array of Ranges.
  deftail :ranges do |max, total, results=[]|
    count = results.length
    if count >= total
      results
    else
      min = results.last ? results.last.max : 0
      top = (min + (max - min) / (total - count.to_f).ceil)
      ranges(max, total, results + [(min + 1)..(top)])
    end
  end
end

def help
  puts "Usage: #{$0} <children> <factorial>"
  puts
  puts "Calculate a factorial, distributed across a given number of worker " \
       "processes.  The number of worker processes must be smaller than the " \
       "factorial target."
  puts "If the result is longer than 300 digits, a truncated result will be " \
       "printed."
  exit
end

args = ARGV.map(&:to_i)
unless args.length == 2 && args.map { |n| n > 0 }.all? && args.first < args.last
  help
end

# Initialize Shatter for the current process so that it has a Pid.
Shatter.init

# Call Factorial.init in another process, instructing it to return the result
# of computing the factorial to the current Pid.
chip(Factorial, :init, pid, *args)

# Wait to receive the result of the large factorial from the process spawned
# above, then print out the number if its length is less than 300, or else the
# first 40 and last 40 digits of the answer, with the number of digits omitted
# noted.
receive do
  match(:answer, nil) do |_, answer|
    num = answer.to_s
    if num.length > 300
      puts "#{num[0..39]}...(#{num.length-80} more)...#{num[-40..-1]}"
    else
      puts num
    end
  end
end
