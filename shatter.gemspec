$LOAD_PATH.unshift File.expand_path('../lib', __FILE__)
require 'shatter'

Gem::Specification.new do |s|
  s.name = 'shatter'
  s.version = Shatter::VERSION

  s.description = 'Framework to facilitate distributed computing with Ruby'
  s.summary     = 'Distributed ruby library'
  s.authors     = ['Tina Wuest']
  s.email       = 'tina@wuest.me'
  s.homepage    = 'https://gitlab.com/wuest/shatter'

  s.add_runtime_dependency('funtools',           '~>0.7', '>=0.7.1')
  s.add_runtime_dependency('rbnacl',             '~>3.1', '>=3.1.2')
  s.add_runtime_dependency('rbnacl-libsodium',   '~>1.0', '>=1.0.0')

  s.files = `git ls-files lib`.split("\n")

  s.license = 'MIT'
end
